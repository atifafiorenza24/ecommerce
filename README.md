**Tokopaerbe** 

Tokopaerbe is Androide based ecommerce appliaction where the entire concept, design, 
and backend were created by our trainer Mr.Reza. This application has several features 
such as displaying list product, adding items to the cart, wishlist, performing checkout, 
and conducting payment transactions all within a single android application 

Tech Stack :
1. Kotlin 
2. Jetpack Compose 
3. Coroutine
4. Live Data
5. Flow 
6. Dependency Injection (Hilt)
7. Glide
8. Material Design 
9. Shared Preference
10. Room 
11. Retrofit 
12. Chucker 
13. Firebase (Analytics, Remote Config, Crushlytics, Notification)
14. Clean Architecture (MVVM) 
15. Modular 
16. Unit Testing
17. Proguard 
18. Detekt

Link Portofolio :
https://www.canva.com/design/DAFwuWV8dVg/_8onXekv6sHAsnG_w8Lfpg/edit?utm_content=DAFwuWV8dVg&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton